#!/usr/bin/python3
"""
Enlighten Programming Challenge:  Alphabet Soup
https://gitlab.com/enlighten-challenge/alphabet-soup

This original, unaided solution was coded by:
Hank Scott
805-315-1382
henry.e.scott.1@gmail.com
"""

puzzle_input_file = "xword.txt"

# Open the sample data file
with open(puzzle_input_file, "r") as file:
    # Read each row of the file into a list
    lines = file.readlines()

# Parse the grid dimensions into a list containing the height and width numbers
grid_dimentions = lines[0].split("x")
# Get the number of rows
grid_rows = int(grid_dimentions[0])
# Get the number of columns
grid_cols = int(grid_dimentions[1])
# Calculate the list position where the word list will start
first_word_line = grid_rows + 1
# Create lists to store the grid and word lists
grid = []
words = []
# Parse the grid letters into grid[]
for index in range(1, first_word_line):
    # Parse each space-delimited line of letters in the grid into a list of
    # letters and add the new list to grid[]
    grid.append(lines[index].split())
# Parse the word list into word[]
for index in range(first_word_line, len(lines)):
    # Strip off any spaces or new-line characters from each word and add them
    # to the word list
    words.append(lines[index].strip())

# Test parsing
# print(str(grid_rows) + "x" + str(grid_cols))
# for row in grid:
#     print(" ".join(row))
# for word in words:
#     print(word)

def within_bounds(grid, row, col):
    """
    Determines if a grid coordinate is within the specified grid
    :param list grid: List of grid row lists
    :param int row: The row number of the grid to check
    :param str col: The column number of the grid to check
    :return bool: True for within bounds and False for out of bounds
    """
    if row >= 0 and col >= 0 and row < len(grid) and col < len(grid[0]):
        return True
    else:
        return False


def next_letter_sweep(grid, coord, word):
    """
    Given a grid, word, and starting coordinates of the word, checks all the
    adjacent coordinates for the second letter of the word.  All direction
    (row and column offset values) used to get to that second letter, and the 
    original word and its starting coordinates, are returned as a list of
    dictionaries.
    :param list grid: List of grid row lists
    :param list coord: A list in [row,col] format indicating the word starting
                       position
    :param str word: The word to check
    :return list: Returns a list of one or more dictionaries containing the
                  original word and starting coordinator as well as the row and
                  column offset direction values that get to the second letter
                  of the word and possibly, if continued, would result in the
                  full word
    """
    # Parse the word's first letter coordinates
    row_first_letter = coord[0]
    col_first_letter = coord[1]
    # Define the list variable to store the dictionaries of words and the
    # directions that lead to the next letter of each word
    direction_list = []
    # create a range to loop through from -1 through 1 for column offset
    for col_shift in range(-1, 2):
         # create a range to loop through from -1 through 1 for row offset
        for row_shift in range(-1, 2):
            # Calculate new coordinates based on the row/col offset
            second_letter_row = row_first_letter + row_shift
            second_letter_col = col_first_letter + col_shift
            # Verify the new coordinate is still in the bounds of the grid
            if within_bounds(grid, second_letter_row, second_letter_col):
                # Check if the letter at the new coordinate is the second letter
                # of the current word being checked and append it to the
                # direction_list of possible paths the word could be found in
                if grid[second_letter_row][second_letter_col] == word[1]:
                    direction_list.append(
                        {
                            "word": word,
                            "start_row": coord[0],
                            "start_col": coord[1],
                            "possible_row_shift": row_shift,
                            "possible_col_shift": col_shift,
                        }
                    )
    # Return the completed direction_list of possible paths the word could
    # be found in
    return direction_list

# Define the global list variable where possible lead dictionaries will be
# stored
possible_leads = []

def check_rows(grid, words):
    """
    Given a grid and list of words, find the first letter of each found word and
    run next_letter_sweep() to evaluate if the next letter of the word is
    nearby.
    :param list grid: List of grid row lists
    :param list words: A list of words
    """
    # Iterate through each word in the word list
    for word in words:
        # Iterate through each row in the grid
        for grid_row_index, grid_row in enumerate(grid):
            # Define the first letter of the current word in the loop
            first_word_letter = word[0]
            # Check if the first letter of the word is found in the current row
            if first_word_letter in grid_row:
                # If the letter is found in the grid row, use list comprehension
                # to get the list of row positions where the letter is found
                found_letter_in_cols = [
                    index
                    for index, item in enumerate(grid_row)
                    if first_word_letter in item
                ]
                # Iterate through each found letter and pass the coordinate of
                # Each found first letter and the word itself to the
                # next_letter_sweep() function for further evaluation
                for column in found_letter_in_cols:
                    coord = [grid_row_index, column]
                    possible_leads.extend(next_letter_sweep(grid, coord, word))


def directional_word_finder(grid, leads):
    """
    Given a grid and a list of words, initial starting coordinates, and possible
    row and column offset values to check, loops through all the possibilities
    and prints out the start and end coordinates for words that are fully
    tracked down.
    :param list grid: List of grid row lists
    :param list coord: A list in [row,col] format indicating the word starting
                       position
    :param str word: The word to check
    :return list: Returns a list of one or more dictionaries containing the
                  original word, starting coordinates, and the row and
                  column offset direction values that get to the second letter
                  of the word and, if continued, could possibly result in the
                  full word
    """
    # Iterate through the possible start coordinates and directions for each
    # word returned in the "possible leads" list
    for lead in leads:
        # Extract the initial start row/col coords for the current lead as well
        # as the current word to find
        next_letter_row = lead.get("start_row")
        next_letter_col = lead.get("start_col")
        word = lead.get("word")
        # Iterate through each letter of the current lead's word
        for index, letter in enumerate(word):
            # Verify coordinate is within the bounds of the grid
            if within_bounds(grid, next_letter_row, next_letter_col):
                # Determine if letter at the current grid coordinates matches
                # The letter at the current index of the current word
                if grid[next_letter_row][next_letter_col] == word[index]:
                    # Determined if we are at the end of the current word
                    if index == len(word) - 1:
                        # Format and print the required output in this format:
                        #   <WORD> <start_row>:<start_col> <end_row>:<end_col>
                        print(
                            word
                            + " "
                            + str(lead.get("start_row"))
                            + ":"
                            + str(lead.get("start_col"))
                            + " "
                            + str(next_letter_row)
                            + ":"
                            + str(next_letter_col)
                        )
                    # Shift the row/col coords based on the offset values in the
                    # current lead to see if the next letter of the current word
                    # will be found in the next loop
                    next_letter_row = next_letter_row + \
                        lead.get("possible_row_shift")
                    next_letter_col = next_letter_col + \
                        lead.get("possible_col_shift")
                else:
                    # If letter found at the next grid coordinates is not the
                    # expected letter, stop the loop for this word lead
                    break
            else:
                # If we will go out of grid bounds in this lead direction,
                # stop the loop for this word lead
                break

# Based on first letter of each word to find in the grid, determine if the
# letter is in each row and record its position. Sweep around each found first
# letter, make a list of words and directions the second letter is found, and
# populate the "possible_leads" list
check_rows(grid, words)
# Use each word's starting position and direction in the "possible_leads" list
# to focus efforts in finding the rest of the word.  Report the beginning and
# end of each word in the required format
directional_word_finder(grid, possible_leads)
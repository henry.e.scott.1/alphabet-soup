# Alphabet Soup Code Challenge

Python3 solution created by Hank Scott.

For more details on the code challenge, refer to the [original repository](https://gitlab.com/enlighten-challenge/alphabet-soup).

## Table of Contents
[[_TOC_]] 

## Prerequisites

1. Clone this repository using your preferred Git tool or use the following command in your CLI:

   ```sh
   git clone https://gitlab.com/henry.e.scott.1/alphabet-soup.git
   ```

2. Check out the `python` branch:

   ```sh
   cd alphabet-soup
   git checkout python
   ```

## Running the Solution

To run the solution locally, follow these steps:

1. From the command line, navigate to the `solution` directory.

2. chmod the `xword.py` file and run the solution

   ```sh
   chmod 775 xword.py
   ./xword.py
   ```

2. Once the script runs, the crossword solution will print to stdout.

## Updating Crossword Puzzle Input

There are two methods to update the crossword puzzle data:

### Method 1

1. Change and save content of `xword.txt`
2. Re-run the script

### Method 2

1. Copy a new crossword puzzle input file to the same directory with `xword.py`
2. Edit the `puzzle_input_file` variable in `xword.py` to be the name of the new file
3. Re-run the script

## Reviewing the Solution

This application has the following file structure:
```
alphabet-soup/solution
├── xword.py
├── README.md
└── xword.txt
```